package semana6;

public class Curso {
	public int codigo;
	public String nombre;
	private int durSemanas;
	private int alumnosMatriculados;
	private int alumnosExamen;
	
	public Curso () {
		this.codigo = 0;
		this.nombre = "";
		this.durSemanas = 0;
		this.alumnosMatriculados = 0;
		this.alumnosExamen = 0;
	}
	
	public Curso (int codigo, String nombre, int durSemanas, int alumnosMatriculados, int alumnosExamen) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.durSemanas = durSemanas;
		this.alumnosMatriculados = alumnosMatriculados;
		this.alumnosExamen = alumnosExamen;
	}
	
	public double porcentajeExamen() {
		double resultado = 0;
		resultado = alumnosExamen * 100 / alumnosMatriculados;
		
		return resultado;
	}

	//interpreto que para codigo y nombre no se necesitan getters y setters porque son p�blicos (no lo logro desprender de la letra)

	public int getDurSemanas() {
		return durSemanas;
	}

	public void setDurSemanas(int durSemanas) {
		this.durSemanas = durSemanas;
	}

	public int getAlumnosMatriculados() {
		return alumnosMatriculados;
	}

	public void setAlumnosMatriculados(int alumnosMatriculados) {
		this.alumnosMatriculados = alumnosMatriculados;
	}

	public int getAlumnosExamen() {
		return alumnosExamen;
	}

	public void setAlumnosExamen(int alumnosExamen) {
		this.alumnosExamen = alumnosExamen;
	}
	
	

}
