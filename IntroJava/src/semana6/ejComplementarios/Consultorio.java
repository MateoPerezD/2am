package semana6.ejComplementarios;

public class Consultorio {
	private String numConsultorio;
	private String direccion;
	private String doctor;
	private Paciente [] pacientes = new Paciente[100] ;
	private int ultimoPaciente;
	
	public Consultorio() {
	}
	
	public Consultorio (String numConsultorio, String direccion, String doctor) {
		this.numConsultorio=numConsultorio;
		this.direccion=direccion;
		this.doctor=doctor;
	}
	
	public void agregarPaciente(Paciente p) {
		this.pacientes[ultimoPaciente]=p;
		ultimoPaciente++;
		
	}
	public Paciente[] listarPacientes(){
		Paciente [] resultado = new Paciente [ultimoPaciente];
		if (ultimoPaciente>0) {
			for (int i=0;i<ultimoPaciente;i++) {
				resultado [i] = pacientes[i];
		}
		}
		return resultado;
	}

	public String getNumConsultorio() {
		return numConsultorio;
	}

	public void setNumConsultorio(String numConsultorio) {
		this.numConsultorio = numConsultorio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public Paciente[] getPacientes() {
		return pacientes;
	}

	public void setPacientes(Paciente[] pacientes) {
		this.pacientes = pacientes;
	}
	
	

}
