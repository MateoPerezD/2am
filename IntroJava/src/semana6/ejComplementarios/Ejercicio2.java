package semana6.ejComplementarios;

public class Ejercicio2 {

	
	public static void main(String[] args) {
		Empleado emp1 = new Empleado ("1","juan","35485555");
		emp1.setValorHora(200.5);
		emp1.cargarHorasMes(1, 160);
		emp1.cargarHorasMes(2, 140);
		emp1.cargarHorasMes(3, 160);
		emp1.cargarHorasMes(4, 150);
		emp1.cargarHorasMes(5, 165);
		emp1.cargarHorasMes(6, 170);
		
		int [] tuvieja = emp1.getHorasPorMes();
		
		emp1.setHorasPorMes(tuvieja);
		
		
		Empleado emp2 = new Empleado ("2","Paula","38887776");
		emp2.setValorHora(250.5);
		emp2.cargarHorasMes(1, 165);
		emp2.cargarHorasMes(2, 135);
		emp2.cargarHorasMes(3, 170);
		emp2.cargarHorasMes(4, 145);
		emp2.cargarHorasMes(5, 160);
		emp2.cargarHorasMes(6, 165);
		
		System.out.println(emp1.getNombre() + " " + emp1.getSueldoMes(6));
		
		
	
	
	}

}
