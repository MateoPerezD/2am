package semana7;

public class NoVotaException extends Exception {
	
	public NoVotaException(String mensaje) {
		super(mensaje);
	}

}
