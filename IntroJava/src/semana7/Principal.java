package semana7;


public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Persona persona1 = new Persona("Maria Perez","44024677","","Uruguay",true);
		Persona persona2 = new Persona("Santiago Diaz","","","Chile",true);
		Persona persona3 = new Persona("Jose Martin","33786754","AZA34579","Uruguay",false);
		Persona persona4 = new Persona("Juan Rodriguez", "24568976","CPA57564","Uruguay",true);
		
		Persona[] listaPersonas = {persona1, persona2, persona3, persona4};
		
		
			for (int i =0; i <listaPersonas.length; i++) {
				try {
					listaPersonas[i].puedeVotar();
					System.out.println(listaPersonas[i].getNombre() + " habilitado a votar");
				}catch (Exception ex) {
					System.out.println(listaPersonas[i].getNombre() + " " + ex.getMessage());
				}
				
			}
			
	}
	
}

			
		