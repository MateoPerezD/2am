package semana7;

public class Persona {
	
	private String nombre;
	private String ci;
	private String credencial;
	private String paisOrigen;
	private boolean habilitadoBps;
	
	public Persona () {	
		this.nombre="";
		this.ci="";
		this.credencial="";
		this.paisOrigen="";
		this.habilitadoBps=false;
	}
	
	public Persona (String nombre, String ci, String credencial, String paisOrigen, boolean habilitadoBps) {
		this.nombre=nombre;
		this.ci=ci;
		this.credencial=credencial;
		this.paisOrigen=paisOrigen;
		this.habilitadoBps=habilitadoBps;
	}

	

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getCredencial() {
		return credencial;
	}

	public void setCredencial(String credencial) {
		this.credencial = credencial;
	}

	public String getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public boolean isHabilitadoBps() {
		return habilitadoBps;
	}

	public void setHabilitadoBps(boolean habilitadoBps) {
		this.habilitadoBps = habilitadoBps;
	}
	
	public void puedeVotar() throws NoVotaException{
		if(!this.isHabilitadoBps()) {
			throw new NoVotaException("No puede votar: No est� habilitado por el BPS");
		}
		
		if (this.paisOrigen.equals("Uruguay") && this.credencial.isEmpty()) {
			throw new NoVotaException("No puede votar: Es de uruguay y no tiene credencial"); 
		}
		
		if (!this.paisOrigen.equals("Uruguay") && this.credencial.isEmpty()) {
			throw new NoVotaException ("No puede votar: No es de uruguay y no tiene ci");
		}
	}
	
	

}
