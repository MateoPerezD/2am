package semana7.ejComplementarios;

public interface ICliente {
	
	public double cobrar ();
	
	static final double descuento = 0.2;	

}
