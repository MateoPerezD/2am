package semana7.ejComplementarios;

public class Alumno {
	
	private int codigoAlumno;
	private String nombreCompleto;
	private boolean recibido;
	private boolean tramitoTitulo;
	
	public Alumno () {
		this.codigoAlumno = 0;
		this.nombreCompleto = " ";
		this.recibido = false;
		this.tramitoTitulo = false;
	}
	
	public Alumno (int codigoAlumno, String nombreCompleto, boolean recibido, boolean tramitoTitulo) {
		
		this.codigoAlumno = codigoAlumno;
		this.nombreCompleto = nombreCompleto;
		this.recibido = recibido;
		this.tramitoTitulo = tramitoTitulo;
	}
	
	public String puedoTramitarTitulo() throws YaRetiroException, NoRecibidoException{
		String mensaje;
		
		if (!this.isRecibido()) {
			throw new YaRetiroException("El usuario ya recibi� su t�tulo");
		}
		
		if (this.isTramitoTitulo()) {
			throw new NoRecibidoException ("El usuario no se recibi� a�n");
		}
		
		else {
			mensaje = "Puede retirar el t�tulo";
		}
		
		return mensaje;
	}

	public int getCodigoAlumno() {
		return codigoAlumno;
	}

	public void setCodigoAlumno(int codigoAlumno) {
		this.codigoAlumno = codigoAlumno;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public boolean isRecibido() {
		return recibido;
	}

	public void setRecibido(boolean recibido) {
		this.recibido = recibido;
	}

	public boolean isTramitoTitulo() {
		return tramitoTitulo;
	}

	public void setTramitoTitulo(boolean tramitoTitulo) {
		this.tramitoTitulo = tramitoTitulo;
	}

}
