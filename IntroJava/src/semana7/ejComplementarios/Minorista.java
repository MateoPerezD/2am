package semana7.ejComplementarios;

public class Minorista implements ICliente {
	private int cantidad;
	private double precio;
	
	public Minorista(int cantidad, int precio) {
		this.cantidad=cantidad;
		this.precio=precio;
	}
	
	public double cobrar() {
		return this.cantidad * this.precio;
	}

}
