package semana7.ejComplementarios;

public class Mayorista implements ICliente {
	private int cantidad;
	private double precio;
	
	public Mayorista(int cantidad, int precio) {
		this.cantidad=cantidad;
		this.precio=precio;
	}
	
	public double cobrar() {
		return this.cantidad * this.precio * this.descuento ;
	}

}
