package semana3;

public class CalcularPromedio {

	public static void main(String[] args) {

		int num1 = Integer.valueOf(args[0]);
		int num2 = Integer.valueOf(args[1]);
		int num3 = Integer.valueOf(args[2]);
		
		// de aqu� para abajo, colocar la implementacion pedida
		
		int numPromedio = (num1 + num2 + num3) /3;
		
		if (numPromedio < 10) {
			System.out.println(numPromedio);
		// la letra no especifica el comportamiento esperado a valor igual a 10, se asume este CU como mayor a 10
		} else {
			int granTotal = numPromedio + num1 + num2 + num3;
			System.out.println(granTotal);
		}

	}

}