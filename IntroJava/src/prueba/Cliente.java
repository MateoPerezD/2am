package prueba;

import java.util.LinkedList;

public class Cliente {
	private String cedula;
	private String nombre;
	private LinkedList<Producto> listaProductosComprados;
	
	public Cliente(String cedula, String nombre) {
		this.cedula = cedula;
		this.nombre = nombre;
		this.listaProductosComprados = new LinkedList<Producto>();
	}
	
	public int registrarCompraProducto (Producto producto) throws Exception {
		int totalComprado = 0;
		if(producto.getStock() == 0) {
			throw new Exception ("El producto no se encuentra en stock");
		}
		producto.setStock(producto.getStock() -1);
		this.listaProductosComprados.add(producto);
		totalComprado = this.listaProductosComprados.size();
		return totalComprado;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Producto> getListaProductosComprados() {
		return listaProductosComprados;
	}

	public void setListaProductosComprados(LinkedList<Producto> listaProductosComprados) {
		this.listaProductosComprados = listaProductosComprados;
	}

}
