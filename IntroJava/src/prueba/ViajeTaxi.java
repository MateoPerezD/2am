package prueba;

public class ViajeTaxi extends Viaje{
	private static final double COSTO_X_KM = 10;
	private static final double COSTO_X_MINUTO = 4.5;
	
	public ViajeTaxi(String codigo, double distancia, int duracion) {
		super (codigo, distancia, duracion);
		//No es necesario incluir en el constructor las constantes, ya que las mismas no van a cambiar
	}
	public double getCostoTotal() {
		double costoTotal = 0;
		double totalDistancia = this.getDistancia() * COSTO_X_KM;
		double totalTiempo = this.getDuracion() * COSTO_X_MINUTO;
		costoTotal = totalDistancia + totalTiempo;
		return costoTotal;
	}
	//La letra no pide getters y setters para los atributos de esta clase, entiendo que es porque son constantes
	

}
