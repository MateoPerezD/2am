package prueba;

public class Viaje {
	
	private String codigo;
	private double distancia;
	private int duracion;
	
	public Viaje (String codigo, double distancia, int duracion) {
		this.codigo = codigo;
		this.distancia = distancia;
		this.duracion = duracion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

}
