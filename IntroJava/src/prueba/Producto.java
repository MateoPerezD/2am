package prueba;

public class Producto {
	private String codigo;
	private String nombre;
	private int stock;
	
	public Producto (String codigo, String nombre, int stock) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.stock = stock;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}
