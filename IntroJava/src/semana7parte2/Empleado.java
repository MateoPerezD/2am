package semana7parte2;

public class Empleado {
	
	private String nombre;
	private String apellido;
	private int nroBPS;
	private int nroFuncionario;
	private String direccion;
	private String telefono;
	
	public Empleado (String nombre,String apellido,int nroBPS,int nroFuncionario,String direccion,String telefono) {
		this.nombre = nombre;
		this.apellido=apellido;
		this.nroBPS=nroBPS;
		this.nroFuncionario=nroFuncionario;
		this.direccion=direccion;
		this.telefono=telefono;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getNroBPS() {
		return nroBPS;
	}

	public void setNroBPS(int nroBPS) {
		this.nroBPS = nroBPS;
	}

	public int getNroFuncionario() {
		return nroFuncionario;
	}

	public void setNroFuncionario(int nroFuncionario) {
		this.nroFuncionario = nroFuncionario;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public double getSueldo() {
		double sueldo = 0;
		return sueldo;
	}

}

