package semana7parte2;

public class EmpJornalero extends Empleado {
	private double valorHora;
	private double horasMes;
	
	public EmpJornalero (String nombre,String apellido,int nroBPS,int nroFuncionario,String direccion,String telefono, double valorHora, double horasMes){
		super (nombre, apellido, nroBPS, nroFuncionario, direccion, telefono);
		this.valorHora = valorHora;
		this.horasMes = horasMes;
		
	}

	public double valorHora() {
		return valorHora;
	}

	public void setValorHora(double valorHora) {
		this.valorHora = valorHora;
	}

	public double getHorasMes() {
		return horasMes;
	}

	public void getHorasMes(double horasMes) {
		this.horasMes = horasMes;
	}
	
	public double getSueldo() {
		double sueldo = this.valorHora * this.horasMes;
		return sueldo;
	}


}
