package semana7parte2;

public class EmpMensual extends Empleado {
	private double salarioMensual;
	private String tipoRegimen;
	
	public EmpMensual (String nombre,String apellido,int nroBPS,int nroFuncionario,String direccion,String telefono, double salarioMensual, String tipoRegimen){
		super (nombre, apellido, nroBPS, nroFuncionario, direccion, telefono);
		this.salarioMensual = salarioMensual;
		this.tipoRegimen = tipoRegimen;
		
	}

	public double getSalarioMensual() {
		return salarioMensual;
	}

	public void setSalarioMensual(double salarioMensual) {
		this.salarioMensual = salarioMensual;
	}

	public String getTipoRegimen() {
		return tipoRegimen;
	}

	public void setTipoRegimen(String tipoRegimen) {
		this.tipoRegimen = tipoRegimen;
	}
	
	public double getSueldo() {
		double sueldo = this.salarioMensual;
		return sueldo;
	}

}
