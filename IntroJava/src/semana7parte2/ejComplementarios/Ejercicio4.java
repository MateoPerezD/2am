package semana7parte2.ejComplementarios;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Estudiante Est1 = new Estudiante (1, "Juan Ramon");
		Estudiante Est2 = new Estudiante (2, "Romina Pases");
		Estudiante Est3 = new Estudiante (3, "Ricardo Pay");
		Estudiante Est4 = new Estudiante (4, "Eliana Guer");
		
		Curso Curso1 = new Curso (1, "Java");
		Curso Curso2 = new Curso (2, "BDatos");
		
		Curso1.agregarEstudiante(Est1);
		Curso1.agregarEstudiante(Est2);
		Curso2.agregarEstudiante(Est3);
		Curso2.agregarEstudiante(Est4);
		
		Docente Docente1 = new Docente (1,"Roberto Can");
		
		Docente1.agregarCursos(Curso1);
		Docente1.agregarCursos(Curso2);
		
		System.out.println(Docente1.getNombre()+" "+Docente1.getListCursosDicta());
		System.out.println(Curso1.getNombre()+" "+Curso1.getEstudiantes());
		System.out.println(Curso2.getNombre() + " "+Curso2.getEstudiantes());
		

	}

}
