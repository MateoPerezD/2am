package semana7parte2.ejComplementarios;

public class Estudiante {
	private int numEstudiante;
	private String nombre;
	
	public Estudiante (int numEstudiante, String nombre) {
		this.numEstudiante = numEstudiante;
		this.nombre = nombre;
	}

	public int getNumEstudiante() {
		return numEstudiante;
	}

	public void setNumEstudiante(int numEstudiante) {
		this.numEstudiante = numEstudiante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
