package semana7parte2;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Sucursal suc1= new Sucursal ("Casa Central", 1, "Montevideo", "28082888", "259142368838");
		Sucursal suc2 = new Sucursal ("Sucursal Durazno", 3, "Durazno", "25823698", "248648282536");
		
		EmpJornalero jorna1 = new EmpJornalero ("Juan","Zabala",328672,68,"Durazno","26223858",215.5,120);
		EmpJornalero jorna2 = new EmpJornalero ("Pedro","Rosen",669825,30,"Montevideo","2852639",280,100.5);
		
		EmpMensual mens1 = new EmpMensual ("Romina","Paito",333458,50,"Durazno","2528655",38000,"Comercial");
		EmpMensual mens2 = new EmpMensual ("Lucas","Manzi",225698,45,"Montevideo","2887469",38500,"Comercial");
		
		suc1.agregarEmpleado(jorna2);
		suc1.agregarEmpleado(mens2);
		
		suc2.agregarEmpleado(jorna1);
		suc2.agregarEmpleado(mens1);
		
		double salida1 = 0;
		double salida2 = 0;
		for (Empleado e1 : suc1.getListaEmpleados()) {
			salida1 = salida1 + e1.getSueldo();
			
		}
		for (Empleado e2 : suc2.getListaEmpleados()) {
			salida2 = salida2 + e2.getSueldo();
			
		}
		
		System.out.println("Gastos por conceptos de Sueldo Sucursal 1 � "+salida1);
		System.out.println("Gastos por conceptos de Sueldo Sucursal 2 � "+salida2);

	}

}
